"""
Created by Owen Traubert for EECE 5136
"""

import random
import numpy as np


LABELS = "MNISTnumLabels5000_balanced.txt"
IMAGES = "MNISTnumImages5000_balanced.txt"
APPENDED_NUM = "appendedNum.txt"
NEURAL_NET_NUM = "nnNum.txt"

TEST_PROPORTION = 0.2




class DataUnpacker:
    def __init__(self):
      
        

        self.numbers = [[[],[]],[[],[]],[[],[]],[[],[]],[[],[]],[[],[]],[[],[]],[[],[]],[[],[]],[[],[]]]
      
        
    def generateDataset(self):
        #get all labels
        labelFile = open(LABELS)
        labelLinesRaw = labelFile.readlines()
        labelLines = []
        for line in labelLinesRaw:
            labelLines.append(int(line))
        labelFile.close()
        
        
        #get all image data
        imageFile = open(IMAGES)
        imageLinesRaw = imageFile.readlines()
        imageLines = []
        for line in imageLinesRaw:
            splitLine = line.split("\t")
            tempLine = []
            for value in splitLine:
                tempLine.append(float(value.strip('\n')))
            imageLines.append(tempLine)
        imageFile.close()
        
        
        #put all data in the appropriate category
       
        categories = [[],[],[],[],[],[],[],[],[],[]]
        

        for i in range(len(imageLines)):
            
            pointIdentity = labelLines[i]
            categories[pointIdentity].append(imageLines[i])
                
        
        #randomly divide the data into training and test
        for i in range(len(categories)):
            trainingSet = categories[i].copy()
            testSet = []
            length = len(trainingSet)
            for j in range(int(length*TEST_PROPORTION)):
                index = random.randrange(0, len(trainingSet))
                self.numbers[i][1].append(trainingSet.pop(index))
                
            self.numbers[i][0] = trainingSet
        
    def saveDataset(self):
        appendedNumFile = open(APPENDED_NUM, "r")
        self.appendedNum = int(appendedNumFile.readline()) + 1
        appendedNumFile.close
        appendedNumFile = open(APPENDED_NUM, "w")
        appendedNumFile.write(str(self.appendedNum))
        appendedNumFile.close
                
        outputLabelsTraining = open("trainingLabels" + str(self.appendedNum)+".txt", "w")
        outputLabelsTest = open("testLabels" + str(self.appendedNum)+".txt", "w")
        outputDataTraining = open("trainingData" + str(self.appendedNum)+".txt", "w")
        outputDataTest = open("testData" + str(self.appendedNum)+".txt", "w")

        for i in range(len(self.numbers)):
            labelArrayTraining = list(np.full(len(self.numbers[i][0]), i))
            labelArrayTest = list(np.full(len(self.numbers[i][1]), i))
            for j in range(len(labelArrayTraining)):

                for k in range(len(self.numbers[i][0][j])):
                    addChar = ""
                    if(k == len(self.numbers[i][0][j]) - 1):
                        if(not(j == len(labelArrayTraining)-1 and i == len(self.numbers) - 1)):
                            addChar = '\n'
                    else:
                        addChar = '\t'  
                    
                    
                    outputDataTraining.write(str(self.numbers[i][0][j][k]) + addChar)
                if(i == len(self.numbers) - 1 and j == len(labelArrayTraining) - 1):
                    outputLabelsTraining.write(str(labelArrayTraining[j]))
                else:
                    outputLabelsTraining.write(str(labelArrayTraining[j])+ '\n')




            
            for j in range(len(labelArrayTest)):
                for k in range(len(self.numbers[i][1][j])):
                    addChar = ""
                    if(k == len(self.numbers[i][1][j]) - 1):
                        if(not(j == len(labelArrayTest)-1 and i == len(self.numbers)-1)):
                            addChar = '\n'
                    else:
                        addChar = '\t'  
                    outputDataTest.write(str(self.numbers[i][1][j][k]) + addChar)                    
                if(i == len(self.numbers) - 1 and j == len(labelArrayTest) - 1):
                    outputLabelsTest.write(str(labelArrayTest[j]))
                else:
                    outputLabelsTest.write(str(labelArrayTest[j])+ '\n')
                    
                
    def openDataset(self, datasetNum):
        self.usedDataset = datasetNum
        labelFileTraining = open("trainingLabels" + str(datasetNum) + ".txt")
        labelLinesRawTraining = labelFileTraining.readlines()
        self.labelLinesTraining = []
        for line in labelLinesRawTraining:
            self.labelLinesTraining.append(int(line))
        labelFileTraining.close()
            
        labelFileTest = open("testLabels" + str(datasetNum) + ".txt")
        labelLinesRawTest = labelFileTest.readlines()
        
        self.labelLinesTest = []
        for line in labelLinesRawTest:
            self.labelLinesTest.append(int(line))
        labelFileTest.close()
        
        imageFileTraining = open("trainingData" + str(datasetNum) + ".txt")
        imageLinesRawTraining = imageFileTraining.readlines()
        self.imageLinesTraining = []
        for line in imageLinesRawTraining:
            splitLine = line.split("\t")
            tempLine = []
            for value in splitLine:
                tempLine.append(float(value.strip('\n')))
            self.imageLinesTraining.append(tempLine)
        imageFileTraining.close()
        
        imageFileTest = open("testData" + str(datasetNum) + ".txt")
        imageLinesRawTest = imageFileTest.readlines()
        self.imageLinesTest = []
        for line in imageLinesRawTest:
            splitLine = line.split("\t")
            tempLine = []
            for value in splitLine:
                tempLine.append(float(value.strip('\n')))
            self.imageLinesTest.append(tempLine)
    
    def returnTrainingSet(self):
        return(self.imageLinesTraining, self.labelLinesTraining)
    
    def returnTestSet(self):
        return(self.imageLinesTest, self.labelLinesTest)
    
        imageFileTest.close()
        
        
    def saveNN(self, neuralNetInfo):
        nnNumFile = open(NEURAL_NET_NUM, "r")
        self.nnNum = int(nnNumFile.readline()) + 1
        nnNumFile.close
        nnNumFile = open(NEURAL_NET_NUM, "w")
        nnNumFile.write(str(self.nnNum))
        nnNumFile.close
        
        neuralNetInfo = list(neuralNetInfo)
        

        layerMatrices = neuralNetInfo.pop()
        layerListing = neuralNetInfo.pop()
        
        neuralNetInfo.append(self.usedDataset)

        nnSaveFile = open("neuralNetSaveFile" + str(self.nnNum) + ".txt", "w")
        
        for i in range(len(neuralNetInfo)):
            appendedChar = '\t'
            if(i == len(neuralNetInfo)-1): appendedChar = '\n'
            nnSaveFile.write(str(neuralNetInfo[i])+appendedChar)
            
        for i in range(len(layerListing)):
            appendedChar = '\t'
            if(i == len(layerListing)-1): appendedChar = '\n'
            nnSaveFile.write(str(layerListing[i])+appendedChar)
            
        for i in range(len(layerMatrices)):
            for j in range(len(layerMatrices[i])):
                for k in range(len(layerMatrices[i][j])):
                    appendedChar = ''
                    if(k < len(layerMatrices[i][j]) - 1):
                        appendedChar = '\t'
                    elif(i == len(layerMatrices) -1 and j == len(layerMatrices[i]) - 1):
                        appendedChar = ''
                    else:
                        appendedChar = '\n'
                    nnSaveFile.write(str(layerMatrices[i][j][k]) + appendedChar)
                
        
        nnSaveFile.close()
                    
        
    def openNN(self, nnNum):
        with open("neuralNetSaveFile" + str(nnNum) + ".txt", "r") as nnSaveFile:
            allLines = [line.rstrip() for line in nnSaveFile]
        
        
        neuralNetInfoRaw = allLines.pop(0).split('\t')
        #self.INPUTS, self.epochNumber, self.ETA, self.testInterval, self.learningGoal, self.useTanH, self.useMomentum, self.useBatch, self.useClassifyMode, self.useOnline
        
        dataFile = int(neuralNetInfoRaw[-1])
        INPUTS = int(neuralNetInfoRaw[0])
        epochNumber = int(neuralNetInfoRaw[1])
        ETA = float(neuralNetInfoRaw[2])
        testInterval = int(neuralNetInfoRaw[3])
        learningGoal = float(neuralNetInfoRaw[4])
        useTanH = int(neuralNetInfoRaw[5])
        useMomentum = int(neuralNetInfoRaw[6])
        useBatch = int(neuralNetInfoRaw[7])
        useClassifyMode = int(neuralNetInfoRaw[8])
        useOnline = int(neuralNetInfoRaw[9])
        
        
        indexToUse = 10
        
        A = None
        B = None
        TANH_HIGH = None
        TANH_LOW = None
        SIGMOID_HIGH = None
        SIGMOID_LOW = None
        if(useTanH):
            A = float(neuralNetInfoRaw[10])
            B = float(neuralNetInfoRaw[11])
            TANH_HIGH = float(neuralNetInfoRaw[12])
            TANH_LOW = float(neuralNetInfoRaw[13])
            indexToUse += 4
        else:
            SIGMOID_HIGH = float(neuralNetInfoRaw[10])
            SIGMOID_LOW = float(neuralNetInfoRaw[11])
            indexToUse += 2
        
        ALPHA = None
        if(useMomentum):
            ALPHA = float(neuralNetInfoRaw[indexToUse])
            indexToUse += 1
            
        BATCH_POINTS = None
        if(useBatch):
            BATCH_POINTS = int(neuralNetInfoRaw[indexToUse])
            indexToUse += 1
            
        layerListingRaw = allLines.pop(0).split('\t')
        layerListing = []
        
        for element in layerListingRaw:
            layerListing.append(int(element))
            
        layerMatrices = []
        
        for i in range(len(layerListing)):
            weightLists = []
            for j in range(layerListing[i]):
                weightListRaw = allLines.pop(0).split('\t')
                weightList = []
                for element in weightListRaw:
                    weightList.append(float(element))
                weightLists.append(weightList)
            
            weightMatrix = np.stack(weightLists)
            layerMatrices.append(weightMatrix)
            
        basicParams = [INPUTS, epochNumber, ETA, testInterval, learningGoal, useTanH, useMomentum, useBatch, useClassifyMode, useOnline, dataFile]
        advancedParams = [A, B, TANH_HIGH, TANH_LOW, SIGMOID_HIGH, SIGMOID_LOW, ALPHA, BATCH_POINTS]

            
        return(basicParams, advancedParams, layerListing, layerMatrices)
                
                    
        
        
        
        
            
        
                        
        
        
        

        
        