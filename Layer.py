import numpy as np
import random

def randomWeightXavier(source, target):
    a = np.sqrt((6)/(source+target))
    randomWeight = random.uniform(-a, a)
    return(randomWeight)

def randomWeight(source):
    a = np.sqrt(3/(source))
    randomWeight = random.uniform(-a, a)
    return(randomWeight)

def initializeWeights(layerSizes, numInputs):
    initializedLayers = []
    numLayers = len(layerSizes)
    for i in range(numLayers):
        if(i):
            #output layer
            if(i == numLayers-1):
                layer = np.empty([layerSizes[i], layerSizes[i-1] + 1], dtype=float)
                for j in range(len(layer)):
                    for k in range(len(layer[j])):
                        layer[j][k] = randomWeight(layerSizes[i-1])
                initializedLayers.append(layer)
            #hidden layers
            else:
                layer = np.empty([layerSizes[i], layerSizes[i-1]+1], dtype=float)
                for j in range(len(layer)):
                    for k in range(len(layer[j])):
                        layer[j][k] = randomWeightXavier(layerSizes[i-1], layerSizes[i+1])
                initializedLayers.append(layer)

        #input layer
        else:
            layer = np.empty([layerSizes[0], numInputs+1], dtype=float)
            for j in range(len(layer)):
                for k in range(len(layer[j])):
                    layer[j][k] = randomWeightXavier(numInputs, layerSizes[1])
            initializedLayers.append(layer)
    return(initializedLayers)
    
def getWeightChangeBlank(layerSizes, numInputs):
    initializedLayers = []
    numLayers = len(layerSizes)
    for i in range(numLayers):
        if(i):
            #output layer
            if(i == numLayers-1):
                layer = np.zeros([layerSizes[i], layerSizes[i-1] + 1], dtype=float)
                initializedLayers.append(layer)
            #hidden layers
            else:
                layer = np.zeros([layerSizes[i], layerSizes[i-1]+1], dtype=float)
                initializedLayers.append(layer)

        #input layer
        else:
            layer = np.zeros([layerSizes[0], numInputs+1], dtype=float)
            initializedLayers.append(layer)
    return(initializedLayers)