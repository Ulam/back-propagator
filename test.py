import DataUnpacker
import Layer
import numpy as np
import random
import matplotlib
import matplotlib.pyplot as plt


'''\
WHY IS THE OUTPUT GOING TO A??????
'''
A = 1.7159
B = 2/3
LAYERS = [784, 200, 10]
INPUTS = 784
TARGET_ONE_HOT_LOW = 0
ETA = 0.01
ALPHA = 0.1

TANH_HIGH = .75
TANH_LOW = -.75

SIGMOID_HIGH = .75
SIGMOID_LOW = .25

BATCH_POINTS = 100

myLayers = Layer.initializeWeights(LAYERS, INPUTS).copy()

#weightChangesMomentum = Layer.getWeightChangeBlank(LAYERS, INPUTS)

epochnumberhack = 0
    


'''
numLayers = len(myLayers)
for i in range(numLayers):
    for j in range(len(numLayers[i])):
        neuralInput
'''





dataUnpacker = DataUnpacker.DataUnpacker()
#dataUnpacker.generateDataset()
#dataUnpacker.saveDataset()
dataUnpacker.openDataset(0)
trainingSet = dataUnpacker.returnTrainingSet()

print(len(trainingSet[0]))


Si = 0
#myInput = np.array(trainingSet[0][0])
#myInput = np.insert(myInput, 0, 1)

#you know what, on second thought, I don't think I was supposed
#to use the J2 loss as a delta standin, probably
def calculateLastLayerDeltas(finalOutputAdjusted, finalOutput, actualIdentity):
    correctOutput = []
    deltas = []
    for i in range(len(finalOutputAdjusted)):
        if(i==actualIdentity):
            correctOutput.append(1)
        else:
            correctOutput.append(TARGET_ONE_HOT_LOW)
    for i in range(len(finalOutputAdjusted)):
       # print(correctOutput[i], finalOutputAdjusted[i])
        difference = None
        if(finalOutputAdjusted[i] == correctOutput[i]):
            difference = 0
        else:
            difference = correctOutput[i] - finalOutput[i]
        #squaredDiff = np.power(difference, 2)
        
        deltas.append(difference)
        
    #print(deltas)
    return(deltas)
    
    
def sigmoidActivation(element):
    return(1/(1+ np.power(np.e, -element)))
        
def sigmoidDerivative(activation):
    return(activation*(1-activation))
        
def tanhActivation(element):
    return(A*np.tanh(B*element))

def tanhDerivative(element):
    return((A*B*np.square(1/np.cosh(B*element))))
        

def forwardPropagate(inputs):
   
    
    activations = []
    derivatives = []
    finalOutput = []
    finalOutputAdjusted = []
    for i in range(len(LAYERS)):
        activation = []
        derivative = []
        myInput = None
    
        #hidden and final layers
        if(i):
            myInput = np.array(activations[i-1])
        
        #input layer
        else:
            myInput = np.array(inputs)
            
        myInput = np.insert(myInput, 0, 1)
        myOutput = np.matmul(myLayers[i], myInput)
            
        for element in myOutput:
            
            activationResult = sigmoidActivation(element) 
            
            activation.append(activationResult)
            derivative.append(sigmoidDerivative(activationResult))
            
        activations.append(activation)
        derivatives.append(derivative)
        
    
    finalOutput = activations[len(LAYERS)-1]
    finalOutputAdjusted = []
    
    for element in finalOutput:
        if(element >= SIGMOID_HIGH):
            finalOutputAdjusted.append(1)
        elif(element <= SIGMOID_LOW):
            finalOutputAdjusted.append(TARGET_ONE_HOT_LOW)
        else:
            finalOutputAdjusted.append(element)
    
    #print(finalOutputAdjusted)
    identifiedCategory = np.argmax(finalOutput)
    
    return(derivatives, activations, finalOutputAdjusted, identifiedCategory, finalOutput)
    
    
    
def backPropagate(derivatives, activations, errorVector, inputs):
    #could the problem have to do with layersDeltas, maybe concerning final layer???
    layersDeltas = []
    weightChangeMatricies = []
    for i in range(len(LAYERS)):
        preliminaryLayerDeltas = None
        #set index to the position of the last layer to work backwards
        index = len(LAYERS)-(i+1)
        
        #if this is not the final layer
        if (i):
            prevLayerDeltas = np.array(layersDeltas[i-1])
            preliminaryLayerDeltas = np.matmul(prevLayerDeltas, np.delete(myLayers[index+1], 0, 1))

        #if dealing with the final layer    
        else:
            preliminaryLayerDeltas = np.array(errorVector)

            

        #print(preliminaryLayerDeltas)
        
        #create an array to hold the deltas for a layer
        layerDeltas = []
        
        #create an array to hold the weight changes for a single neuron
        #this array should be thought of as horizontal
        weightChanges = []
        
        #for each neuron in the layer
        for j in range(len(preliminaryLayerDeltas)):
            
            #the delta for this specific neuron is the sum of the weighted
            #deltas of all the neurons it feeds into times the derivative
            #of the neuron's activation function 
            individualDelta = preliminaryLayerDeltas[j]*derivatives[index][j]
            #print(individualDelta > 0, j, i)
            
            
            #append the value to the list of deltas for this layer
            layerDeltas.append(individualDelta)
            
            #if this is not the first layer of neurons
            if(index):
                #the inputs to this neuron were the outputs of the layer before it
                inputActivations = np.array(activations[index-1])
                
            #if this is the fist layer of neurons
            else:
                #the inputs to this neuron were the inputs to the network
                inputActivations = np.array(inputs)
                
            #insert 1 to the activations because of the 0th input, the bias, always 1
            inputActivations = np.insert(inputActivations, 0, 1)
            
            #ERROR FIXING COMMENT
            #try multiplying the activations by the weights since they'll
            #never be negative in the case of the final layer if not multiplied
            #GOT RID OF THIS I THINK IT'S WRONG
            #inputActivations = inputActivations*myLayers[index][j]
            
            #print(inputActivations)
            
            #does there need to be a minus sign here??
            #the weight change for the neuron should be its delta times
            #eta multiplied elementwise with all of the inputs to the neuron
            weightChange = individualDelta*ETA*inputActivations
            
            #for thing in weightChange:
                #print(thing != 0, i, epochnumberhack)
            #print(i, epochnumberhack)
            #print(weightChange)
            weightChanges.append(weightChange)
        
        #form a matrix by stacking all the weight change arrays together
        weightChangeMatrix = np.stack(weightChanges)
        layersDeltas.append(layerDeltas)
        
        weightChangeMatricies.append(weightChangeMatrix)
        
    weightChangeMatricies.reverse()
    
    return(weightChangeMatricies)
        
                

currentTrainingPoints = []
currentTrainingLabels = []
trainingCopy = trainingSet
for i in range(4000):
    index = random.randint(0, len(trainingCopy[1])-1)
    currentTrainingPoints.append(trainingCopy[0].pop(index))
    currentTrainingLabels.append(trainingCopy[1].pop(index))
    
    

'''
currentTrainingPoints = trainingSet[0][:100]
currentTrainingLabels = trainingSet[1][:100]
    '''

errors = []
for k in range(500):
    
    
    weightChangesMomentum = Layer.getWeightChangeBlank(LAYERS, INPUTS).copy()
    correctIdentifications = 0
    weightChangeAccumulator = Layer.getWeightChangeBlank(LAYERS, INPUTS).copy()
        
    
   #randomize the order the points are shown in 
    currentTrainingPointsCopy = []
    currentTrainingLabelsCopy = []
    while(len(currentTrainingLabels)):
        index = 0
        if(len(currentTrainingLabels)-1): index = random.randint(0, len(currentTrainingLabels)-1)
        currentTrainingPointsCopy.append(currentTrainingPoints.pop(index))
        currentTrainingLabelsCopy.append(currentTrainingLabels.pop(index))
        
          
    
    currentTrainingPoints = currentTrainingPointsCopy
    currentTrainingLabels = currentTrainingLabelsCopy
    
    
    
    #train on 100 points
    for j in range(BATCH_POINTS):
        
        randomSelection = random.randint(0, len(currentTrainingLabels)-1)
        
        #result consists of (derivatives, activations, finalOutputAdjusted, identifiedCategory, finalOutput)
        
        result = forwardPropagate(currentTrainingPoints[j])
        actualCategory = currentTrainingLabels[j]
        
        #calculate the error vector for each point in the output        
        errorVector = calculateLastLayerDeltas(result[2], result[4], actualCategory) 
        
        #increment the number of correct identifications if the point's a match
        if(result[3] == actualCategory):
            correctIdentifications += 1
            
        #back propagate the error
        weightChangeMatricies = backPropagate(result[0], result[1], errorVector, currentTrainingPoints[j])
        
        for i in range(len(weightChangesMomentum)):
            weightChangesMomentum[i] = weightChangeMatricies[i] + ALPHA*weightChangesMomentum[i]
       
        for l in range(len(myLayers)):
            weightChangeAccumulator[l] = weightChangeAccumulator[l]+weightChangesMomentum[l]
    

    #update the weights in the weights for the network and write them to a file for analysis
    for j in range(len(myLayers)):
        #weightFile = open("weightFileLayer" + str(j) + "epoch" + str(epochnumberhack) + ".txt" ,"w")

        myLayers[j] = myLayers[j]+weightChangeAccumulator[j]
        
        '''
        for row in myLayers[j]:
            for element in row:
                weightFile.write(str(element)+"\t")
            weightFile.write("\n")
        
        weightFile.close()
        '''
        
        #for element in (weightChangeAccumulator[j]):
          #  print(element)
        
    error = (1-(correctIdentifications/BATCH_POINTS))
    errors.append(error)
    print(error)
    epochnumberhack +=1

fig, ax = plt.subplots()
ax.plot(np.arange(0,500), errors)
    
    

'''

identifiedCategories = []

for i in range(len(trainingSet[0])):
    result = forwardPropagate(trainingSet[0][i])
    
    errorVector = calculateJ2Vector(result[2], actualCategory) 
    if(not(i)):
        print(errorVector)
    
    identifiedCategories.append(result[3])
    

correctIdentifications = 0



for i in range(len(identifiedCategories)):
    if(identifiedCategories[i] == trainingSet[1][i]):
        correctIdentifications+=1

error = (1-(correctIdentifications/len(identifiedCategories)))

        


 
print(error)
'''

'''        
print(len(activations))
print(activations[-1])
print("\n\n\n\n\n")
print(len(derivatives))
print(derivatives[-1])
#print(Si)
'''