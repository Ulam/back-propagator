import DataUnpacker
import NeuralNet
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import math
import random


fileStringProblem = "HW4_Problem_2_Static_Autoencoder_24_01_"


def saveData(neuralNet, dataUnpacker):
    neuralNetData = neuralNet.exportLayers()
    dataUnpacker.saveNN(neuralNetData)
    
def openData(saveNumber, alternativeDataSetNumber):
    dataUnpacker = DataUnpacker.DataUnpacker()
    saveData = dataUnpacker.openNN(saveNumber)
    
    basicParams = saveData[0]
    advancedParams = saveData[1]
    layers = saveData[2]
    LAYER_MATRICES = saveData[3]
    
    inputs = (basicParams[0])
    epochNumber = (basicParams[1])
    eta = (basicParams[2])
    testInterval = (basicParams[3])
    learningGoal = (basicParams[4])
    useTanH = (basicParams[5])
    useMomentum = (basicParams[6])
    useBatch = (basicParams[7])
    useClassifyMode = (basicParams[8])
    useOnline = (basicParams[9])
    
    dataFileNumber = basicParams[10]
    if(alternativeDataSetNumber > 1):
        dataFileNumber = alternativeDataSetNumber
    
    A = advancedParams[0]   
    B = advancedParams[1]
    TANH_HIGH = advancedParams[2]
    TANH_LOW = advancedParams[3]
    SIGMOID_HIGH = advancedParams[4]
    SIGMOID_LOW = advancedParams[5]
    ALPHA = advancedParams[6]
    BATCH_POINTS = advancedParams[7]
    
    
    
    dataUnpacker.openDataset(dataFileNumber)
    trainingSet = dataUnpacker.returnTrainingSet()
    testSet = dataUnpacker.returnTestSet()
    
    myNeuralNet = NeuralNet.NeuralNet(layers, inputs, eta, testInterval, learningGoal, useTanH, useMomentum, useBatch, useClassifyMode, useOnline, trainingSet, testSet)

    if(useTanH):
        myNeuralNet.setAB(A, B)
        myNeuralNet.setTanHHiLo(TANH_HIGH, TANH_LOW)
    else:
        myNeuralNet.setSigmoidHiLo(SIGMOID_HIGH, SIGMOID_LOW)
        
    if(useMomentum):
        myNeuralNet.setAlpha(ALPHA)
        
    if(useBatch):
        myNeuralNet.setBatchPoints(BATCH_POINTS)
    
    myNeuralNet.importLayers(LAYER_MATRICES)
    
    return(myNeuralNet, dataUnpacker)

def displayNeuronOutput1(outputArray):
    output2d = np.reshape(outputArray, (-1, 28))
    
    fig, ax = plt.subplots()
    ax.imshow(output2d)
    
    
def displayNetOutput1(inputArray, outputArray):
    input2d = np.reshape(inputArray, (28, 28)).T
    output2d = np.reshape(outputArray, (28, 28)).T
    
    fig, ax = plt.subplots(1, 2)
    
    ax[0].axis('off')
    ax[1].axis('off')
    ax[0].imshow(input2d)
    ax[1].imshow(output2d)

def displayNetOutput8(inputArrayArray, outputArrayArray, noiseFrees):
    inputsArray = []
    outputsArray = []
    noiseFreeArray = []
    for element in inputArrayArray:
        inputsArray.append(np.reshape(element, (28,28)).T)
    for element in outputArrayArray:
        outputsArray.append(np.reshape(element,(28,28)).T)
    for element in noiseFrees:
        noiseFreeArray.append(np.reshape(element,(28,28)).T)
    
    #ax2.yaxis.set_tick_params(size =0)
    #ax1.yaxis.tick_left()
    #hacky way so I don't have to pass a bool
    sameBool = 1
    for i in range(len(noiseFrees[0])):
        if(noiseFrees[0][i] != inputArrayArray[0][i]):
            sameBool = 0
            break
        
    if(sameBool):
    
        fig, ax = plt.subplots(2, len(inputsArray))
        fig.set_size_inches(8, 2.5)
        
        for i in range(len(inputsArray)):
            if(not(i)):
                plt.setp(ax[0][i].get_yticklabels(), visible = False)
                plt.setp(ax[1][i].get_yticklabels(), visible = False)
                plt.setp(ax[0][i].get_xticklabels(), visible = False)
                plt.setp(ax[1][i].get_xticklabels(), visible = False)
                ax[0][i].yaxis.set_tick_params(size = 0)
                ax[1][i].yaxis.set_tick_params(size = 0)
                ax[0][i].xaxis.set_tick_params(size = 0)
                ax[1][i].xaxis.set_tick_params(size = 0)
    
    
                ax[0][i].set(ylabel = "Input")
                ax[1][i].set(ylabel = "Output")
            else:
                ax[0][i].axis('off')
                ax[1][i].axis('off')
    
    
            ax[0][i].imshow(inputsArray[i])
            ax[1][i].imshow(outputsArray[i])
            
        plt.savefig(fileStringProblem +"Approximator_No_Noise.png")
    else:
        
        fig, ax = plt.subplots(3, len(inputsArray))
        fig.set_size_inches(8, 3.75)
        for i in range(len(inputsArray)):
            if(not(i)):
                plt.setp(ax[0][i].get_yticklabels(), visible = False)
                plt.setp(ax[1][i].get_yticklabels(), visible = False)
                plt.setp(ax[2][i].get_yticklabels(), visible = False)
                plt.setp(ax[0][i].get_xticklabels(), visible = False)
                plt.setp(ax[1][i].get_xticklabels(), visible = False)
                plt.setp(ax[2][i].get_xticklabels(), visible = False)
                ax[0][i].yaxis.set_tick_params(size = 0)
                ax[1][i].yaxis.set_tick_params(size = 0)
                ax[2][i].yaxis.set_tick_params(size = 0)
                ax[0][i].xaxis.set_tick_params(size = 0)
                ax[1][i].xaxis.set_tick_params(size = 0)
                ax[2][i].xaxis.set_tick_params(size = 0)
    
                ax[0][i].set(ylabel = "Original")
                ax[1][i].set(ylabel = "Noisy Input")
                ax[2][i].set(ylabel = "Output")
            else:
                ax[0][i].axis('off')
                ax[1][i].axis('off')
                ax[2][i].axis('off')
    
            ax[0][i].imshow(noiseFreeArray[i]) 
            ax[1][i].imshow(inputsArray[i])
            ax[2][i].imshow(outputsArray[i])
            
        plt.savefig(fileStringProblem+"Approximator_Noise.png")
        
        


def displayFeatures(neuronWeightSet, classifier):
    display2d = []
    for member in neuronWeightSet:
        display2d.append(np.reshape(member, (28, 28)).T)
    
    fig, ax = plt.subplots(4, int(len(display2d)/4))
    
    print(len(neuronWeightSet))
    for i in range(len(neuronWeightSet)):
        ax[i%4][math.floor(i/4)].axis('off')
        ax[i%4][math.floor(i/4)].imshow(display2d[i])
        
    addString = "Approximator"
    if(classifier): addString = "Classifier"
    
        
    plt.savefig(fileStringProblem + "Feature_Display" + addString + ".png")
    
def graphNumberError(trainingErrors, testErrors, classifier):
    meanTraining = np.mean(trainingErrors)
    meanTest = np.mean(testErrors)
    
    means = [meanTraining, meanTest]
    
    addString = "(J2)"
    fileString = "Approximator"
    if(classifier): 
        addString = "(1 - Balanced Accuracy)"
        fileString = "Classifier"
    
    width = .35
    fig, ax = plt.subplots()
    fig.set_size_inches(4, 4)
    ax.bar([0, 0.65], means, width)
    plt.xticks([0, 0.65], ["Training", "Test"])
    ax.set(ylabel="Error " + addString)
    
    plt.savefig(fileStringProblem + "Mean_Training_Test_Error_"+fileString+".png")
    
    
    fig, ax = plt.subplots()
    
    x = np.arange(10)
    width = 0.35
    
    ax.xaxis.set_ticks(np.arange(10))
    ax.set(ylabel = "Error " + addString)
    rects1 = ax.bar(x - width/2, trainingErrors, width, label="Training", color = "royalblue")
    rects2 = ax.bar(x + width/2, testErrors, width, label = "Test", color = "orange")
    fig.legend(loc="upper right")
    
    plt.savefig(fileStringProblem + "Training_Test_Number_Error"+fileString+".png")

    
    

dataUnpacker = DataUnpacker.DataUnpacker()
#dataUnpacker.generateDataset()
#dataUnpacker.saveDataset()
dataUnpacker.openDataset(1)


layers = [200, 10]
inputs = 784
eta = 0.01
learningGoal = .05
testInterval = 20
useTanH = 0
useMomentum = 1
useBatch = 1
useClassifyMode = 1
useOnline = 0
trainingSet = dataUnpacker.returnTrainingSet()
testSet = dataUnpacker.returnTestSet()



#for the autoencoder
'''
trainingLabels = trainingSet[1]
testLabels = testSet[1]
trainingSet = [trainingSet[0], trainingSet[0]]
testSet = [testSet[0], testSet[0]]
'''


#trainingSet = [trainingSet[0][0:400], trainingSet[1][0:400]]


#change the useTanH back to 0 and get rid of subset when you run this overnight also make sure to SAVE IT
myNeuralNet = NeuralNet.NeuralNet(layers, inputs, eta, testInterval, learningGoal, useTanH, useMomentum, useBatch, useClassifyMode, useOnline, trainingSet, testSet)
#myNeuralNet.setNoise(True, 2, 0.5, 0.075, 0, .8, 0.5)
#myNeuralNet.setAlpha(.2)

#myNeuralNet.setSigmoidHiLo(.85, .15)
#myNeuralNet.setAlpha(.2)



#this bit will open up the trained autoencoder and get the first layer of weights
#and put in the present neural net, then it will set this imported layer not to be changed

myNeuralNetCopy, unpackerCopy = openData(24, -1)
#myNeuralNetCopy.setNoise(True, 2, 0.5, 0.075, 0, .8, 0.5)

#myNeuralNetCopy.setTraining(trainingSet)
#myNeuralNetCopy.setTest(testSet)

weightsToImportRaw = myNeuralNetCopy.exportJustWeights()
desiredWeights = weightsToImportRaw[0:1]
desiredWeightsListing = [0]
myNeuralNet.importSomeLayers(desiredWeights, desiredWeightsListing)
myNeuralNet.setTrainLayers([1])


#myNeuralNet.setTraining(trainingSet)
#myNeuralNet.setTest(testSet)
#trainingErrors, testErrors = myNeuralNet.getNumberErrorTrainingTest(trainingLabels, testLabels)
#graphNumberError(trainingErrors, testErrors, 0)


 



'''
error = myNeuralNet.getTestError()
print(error)
'''
'''
myIn, myOut = myNeuralNet.generateInputOutput()
displayNetOutput1(myIn, myOut)
'''
'''
selectionLayer = 0
mySelection = random.sample(range(layers[selectionLayer]), 20)
weightsSelection = myNeuralNet.generateNeuronFeatureSet(mySelection, selectionLayer)
displayFeatures(weightsSelection, 1)
'''


errors, testErrors = myNeuralNet.train(1500)
saveData(myNeuralNet, dataUnpacker)


'''
fig, ax =plt.subplots()
ax.set(xlabel = "Epoch Number", ylabel = "Error (1 - Balanced Accuracy)")
ax.plot(np.arange(0,len(trainingErrors)), trainingErrors)



fig, ax = plt.subplots()
trainingErrors = trainingErrors[0::10]
xCoords = np.arange(0, len(trainingErrors))
xCoords *= 10
ax.set(xlabel = "Epoch Number", ylabel = "(1 - Balanced Accuracy)")

ax.plot(xCoords, trainingErrors)

fig, ax = plt.subplots()
'''


'''

fig, ax = plt.subplots()
ax.set(xlabel = "Epoch Number", ylabel = "Error (1 - Balanced Accuracy)")
#change the 100 to whatever batch size you're using
ax.plot(np.arange(0,len(testErrors))*testInterval, testErrors)
plt.savefig("Problem_1_Error_Balanced_Accuracy_Test_Data_Correct_Layers_newDataRegularBest200HL.png")


'''







    


# HW 3 problem 2 graph stuff
'''
fig, ax = plt.subplots()
ax.set(xlabel = "Epoch Number", ylabel = "Error (J2 Loss)")
ax.plot(np.arange(0,len(errors)), errors)
plt.savefig(fileStringProblem + "Error_J2_Full.png")


fig, ax = plt.subplots()
errors = errors[0::10]
xCoords = np.arange(0, len(errors))
xCoords *= 10
ax.set(xlabel = "Epoch Number", ylabel = "Error (J2 Loss)")

ax.plot(xCoords, errors)
plt.savefig(fileStringProblem + "Error_J2_10s.png")

fig, ax = plt.subplots()
ax.set(xlabel = "Epoch Number", ylabel = "Error (J2 Loss)")
#change the 100 to whatever batch size you're using
ax.plot(np.arange(0,len(testErrors))*testInterval, testErrors)
plt.savefig(fileStringProblem + "Error_J2.png")




mySelection = random.sample(range(layers[0]), 20)
weightsSelection = myNeuralNet.generateNeuronFeatureSet(mySelection, 0)
displayFeatures(weightsSelection, False)


myInputs = []
myOutputs = []
noiseFrees = []
for i in range(8):
    myIn, myOut, noiseFree = myNeuralNet.generateInputOutput()
    myInputs.append(myIn)
    myOutputs.append(myOut)
    noiseFrees.append(noiseFree)
    
displayNetOutput8(myInputs, myOutputs, noiseFrees)

trainingErrors, testErrors = myNeuralNetCopy.getNumberErrorTrainingTest(trainingLabels, testLabels)
graphNumberError(trainingErrors, testErrors, 0)
'''




'''
myNeuralNet.setBatchPoints(200)
saveData(myNeuralNet, dataUnpacker)
'''
'''
copyNeuralNet, copyDataUnpacker = openData(1, -1)
copyError = copyNeuralNet.getTestError()
print(copyError)
mySelection = random.sample(range(layers[1]), 20)
weightsSelection = copyNeuralNet.generateNeuronFeatureSet(mySelection)
displayFeatures(weightsSelection, 1)
'''


'''
copyError = copyNeuralNet.getTestError()
print(copyError)
saveData(copyNeuralNet, copyDataUnpacker)
'''



#code for HW3 problem 1

fig, ax = plt.subplots()
ax.set(xlabel = "Epoch Number", ylabel = "Error (1 - Balanced Accuracy)")
ax.plot(np.arange(0,len(errors)), errors)
plt.savefig(fileStringProblem + "Balanced_Accuracy_Full.png")


fig, ax = plt.subplots()
errors = errors[0::10]
xCoords = np.arange(0, len(errors))
xCoords *= 10
ax.set(xlabel = "Epoch Number", ylabel = "Error (1 - Balanced Accuracy)")

ax.plot(xCoords, errors)
plt.savefig(fileStringProblem + "Balanced_Accuracy_10s.png")


fig, ax = plt.subplots()
ax.set(xlabel = "Epoch Number", ylabel = "Error (1 - Balanced Accuracy)")
#change the 100 to whatever batch size you're using
ax.plot(np.arange(0,len(testErrors))*testInterval, testErrors)
plt.savefig(fileStringProblem + "Balanced_Accuracy_Test_Data.png")


tableTextTraining = myNeuralNet.getConfusionMatrixNumberClassification(1)
tableTextTest = myNeuralNet.getConfusionMatrixNumberClassification(0)

fig, ax = plt.subplots()
fig.set_size_inches(8.5, 3)
collabel = ["ID 0", "ID 1", "ID 2", "ID 3", "ID 4", "ID 5", "ID 6", "ID 7", "ID 8", "ID 9"]
rowlabel = ["GT 0", "GT 1", "GT 2", "GT 3", "GT 4", "GT 5", "GT 6", "GT 7", "GT 8", "GT 9"]
ax.table(cellText = tableTextTraining, colLabels = collabel, rowLabels = rowlabel, loc="center")
ax.axis('tight')
ax.axis('off')

plt.savefig(fileStringProblem + "Confusion_Matrix_Training.png")


fig, ax = plt.subplots()
fig.set_size_inches(8.5, 3)
collabel = ["ID 0", "ID 1", "ID 2", "ID 3", "ID 4", "ID 5", "ID 6", "ID 7", "ID 8", "ID 9"]
rowlabel = ["GT 0", "GT 1", "GT 2", "GT 3", "GT 4", "GT 5", "GT 6", "GT 7", "GT 8", "GT 9"]
ax.table(cellText = tableTextTest, colLabels = collabel, rowLabels = rowlabel, loc="center")
ax.axis('tight')
ax.axis('off')

plt.savefig(fileStringProblem + "Confusion_Matrix_Test.png")


