import DataUnpacker
import Layer
import numpy as np
import random
import matplotlib
import matplotlib.pyplot as plt
import math

#hacky function please ignore
fileStringProblem = "HW4_Problem_1_Static_Version_2_01_"
def displayNetOutput1(inputArray, outputArray, epochNum, useNoise, noNoiseInput):
    input2d = np.reshape(inputArray, (28, 28)).T
    output2d = np.reshape(outputArray, (28, 28)).T
    
    
    if(useNoise):
            noiseFree2d = np.reshape(noNoiseInput, (28,28)).T
            fig, ax = plt.subplots(1, 3)
            
            ax[0].axis('off')
            ax[1].axis('off')
            ax[2].axis('off')
            ax[1].imshow(input2d)
            ax[0].imshow(noiseFree2d)
            ax[2].imshow(output2d)
            plt.savefig(fileStringProblem + "Noisy_" + str(epochNum)+"_00.png")
        
            plt.show()
    else:
        fig, ax = plt.subplots(1, 2)
        
        ax[0].axis('off')
        ax[1].axis('off')
        ax[0].imshow(input2d)
        ax[1].imshow(output2d)
        plt.savefig(fileStringProblem + "No_Noise_" + str(epochNum)+"_00.png")
    
        plt.show()
    
    
class NeuralNet:
    
    def __init__(self, layers, inputs, eta, testInterval, learningGoal, useTanH, useMomentum, useBatch, useClassifyMode, useOnline, trainingSet, testSet):
        self.LAYERS = layers
        self.INPUTS = inputs
        self.ETA = eta
        self.testInterval = testInterval
        self.learningGoal = learningGoal
        
        self.useTanH = useTanH
        self.useMomentum = useMomentum
        self.useBatch = useBatch
        self.useClassifyMode = useClassifyMode
        self.useOnline = useOnline
        
        self.addNoise = 0
        
        self.trainingSet = trainingSet
        self.testSet = testSet
        
        if(useTanH):
            self.TARGET_ONE_HOT_LOW = -1
            self.A = 1.7159
            self.B = 2/3
            self.TANH_HIGH = .75
            self.TANH_LOW = -.75
            
        else:
            self.TARGET_ONE_HOT_LOW = 0
            self.SIGMOID_HIGH = .75
            self.SIGMOID_LOW = .25
            
        if(useMomentum):
            self.ALPHA = 0.1
        
        if(useBatch):
            self.BATCH_POINTS = 100
        else:
            #if not doing batch mode, show all points in a batch
            self.BATCH_POINTS = len(trainingSet[1])
        
        self.ETA = 0.01

        self.epochNumber = 0
        
        self.LAYER_MATRICES = Layer.initializeWeights(self.LAYERS, self.INPUTS).copy()
        
        self.trainLayers = range(len(layers))
        
        self.CURRENT_BEST = self.LAYER_MATRICES

    def importLayers(self, layerMatrices):
        self.LAYER_MATRICES = layerMatrices
        
    def importSomeLayers(self, layerMatrices, indiciesOfLayer):
        layerIndex = 0
        for i in range(len(self.LAYERS)):
            if(i == indiciesOfLayer[layerIndex]):
                self.LAYER_MATRICES[i] = layerMatrices[layerIndex]
                layerIndex += 1
            if(layerIndex == len(indiciesOfLayer)):
                return

            
        
    def exportJustWeights(self):
        return(self.LAYER_MATRICES)
        
    def exportLayers(self):
        
        basicParams = [self.INPUTS, self.epochNumber, self.ETA, self.testInterval, self.learningGoal, self.useTanH, self.useMomentum, self.useBatch, self.useClassifyMode, self.useOnline]
        if(self.useTanH):
            basicParams.append(self.A)
            basicParams.append(self.B)
            basicParams.append(self.TANH_HIGH)
            basicParams.append(self.TANH_LOW)
        else:
            basicParams.append(self.SIGMOID_HIGH)
            basicParams.append(self.SIGMOID_LOW)
        
        
        if(self.useMomentum):
            basicParams.append(self.ALPHA)
            
        if(self.useBatch):
            basicParams.append(self.BATCH_POINTS)
            
            
        basicParams.append(self.LAYERS)
        basicParams.append(self.CURRENT_BEST)
                
        return(basicParams)
   
                     
                     
        
    def subsetData(self, subsetSize):
        currentTrainingPoints = []
        currentTrainingAnswers = []
        trainingCopy = [list(self.trainingSet[0]).copy(), list(self.trainingSet[1]).copy()]
        for i in range(subsetSize):
            index = random.randint(0, len(trainingCopy[1])-1)
            currentTrainingPoints.append(trainingCopy[0].pop(index))
            currentTrainingAnswers.append(trainingCopy[1].pop(index))
            
        self.trainingSet = [currentTrainingPoints, currentTrainingAnswers]
        
    def setAB(self, A, B):
        self.A = A
        self.B = B
        
    def setAlpha(self, alpha):
        self.ALPHA = alpha
        
    def setBatchPoints(self, batchPoints):
        self.BATCH_POINTS = batchPoints
        
    def setTanHHiLo(self, high, low):
        self.TANH_HIGH = high
        self.TANH_LOW = low
        
    def setSigmoidHiLo(self, high, low):
        self.SIGMOID_HIGH = high
        self.SIGMOID_LOW = low
        
    def setEta(self, eta):
        self.ETA = eta
        
    def setTraining(self, trainingSet):
        self.trainingSet = trainingSet
        
    def setTest(self, testSet):
        self.testSet = testSet
        
    def setNoise(self, noiseBool, noiseMethod, zeroPercent, onePercent, mean, standardDev, snowProportion):
        self.addNoise = noiseBool
        self.noiseMethod = noiseMethod
        if(noiseMethod == 0):
            self.zeroPercent = zeroPercent
            self.onePercent = onePercent
        elif(noiseMethod == 1):
            self.mean = mean
            self.standardDev = standardDev
        elif(noiseMethod == 2):
            self.zeroPercent = zeroPercent
            self.onePercent = onePercent
            self.mean = mean
            self.standardDev = standardDev
            self.snowProportion = snowProportion            
            
    def setTrainLayers(self, layersToTrain):
        self.trainLayers = layersToTrain
        
        
    
    def generateNoise(self, imageToRunOriginal):
        #this code adds noise based on the selected method, either salt and pepper distributed
        #according to a proportion, or a change on each pixel by a +/- number in a normal distribution
        #around the desired mean
        
        imageToRun = imageToRunOriginal.copy()
        
        if(self.noiseMethod == 0):
            for inputIndex in range(len(imageToRun)):
                substitutionList = [0.0, 1.0, imageToRun[inputIndex]]
                probabilities = [self.zeroPercent, self.onePercent, 1-(self.zeroPercent+self.onePercent)]
                imageToRun[inputIndex] = random.choices(substitutionList, weights = probabilities, k=1)[0]
        
        elif(self.noiseMethod == 1):
            for inputIndex in range(len(imageToRun)):
                offset = np.random.normal(self.mean, self.standardDev)
                newValue = imageToRun[inputIndex] + offset
                if(newValue < 0): newValue=0.0
                if(newValue > 1): newValue = 1.0
                imageToRun[inputIndex] = newValue
                
        elif(self.noiseMethod == 2):
            for inputIndex in range(len(imageToRun)):
                
                
                substitutionList = [0.0, 1.0, imageToRun[inputIndex]]
                probabilities = [self.zeroPercent*self.snowProportion, self.onePercent*self.snowProportion, 1-self.snowProportion*(self.zeroPercent+self.onePercent)]
                imageToRun[inputIndex] = random.choices(substitutionList, weights = probabilities, k=1)[0]
                normalOffset = offset = np.random.normal(self.mean, self.standardDev*(1-self.snowProportion))
                newValue = imageToRun[inputIndex] + offset
                if(newValue < 0): newValue=0.0
                if(newValue > 1): newValue = 1.0
                imageToRun[inputIndex] = newValue
                
                
        imageToRun = np.array(imageToRun)
                
        return(imageToRun)
        
    def sigmoidActivation(self, element):
        return(1/(1+ np.power(np.e, -element)))
            
    def sigmoidDerivative(self, activation):
        return(activation*(1-activation))
            
    def tanhActivation(self, element):
        return(self.A*np.tanh(self.B*element))
    
    def tanhDerivative(self, element):
        coshResult = np.cosh(self.B*element)
        sechResult = 1/coshResult
        sech2 = np.square(sechResult)
        return(self.A*self.B*sech2)
    
    
    def calculateLastLayerDeltasClassifier(self, finalOutputAdjusted, finalOutput, actualIdentity):
        correctOutput = []
        deltas = []
        for i in range(len(finalOutputAdjusted)):
            if(i==actualIdentity):
                correctOutput.append(1)
            else:
                correctOutput.append(self.TARGET_ONE_HOT_LOW)
        for i in range(len(finalOutputAdjusted)):
            difference = None
            if(finalOutputAdjusted[i] == correctOutput[i]):
                difference = 0
            else:
                difference = correctOutput[i] - finalOutput[i]
            
            deltas.append(difference)
            
        return(deltas)
    
    def calculateLastLayerDeltasJ2Approximator(self, finalOutput, correctOutput):
        deltas = []
        runningSum = 0
        #print(finalOutput)
        #print(correctOutput)
        for i in range(len(finalOutput)):
            difference = correctOutput[i]-finalOutput[i]
            deltas.append(difference)
            runningSum += np.power(difference, 2)
            
        runningSum /= 2
        return(deltas, runningSum)
    
    def forwardPropagate(self, inputs):
        
        activations = []
        derivatives = []
        finalOutput = []
        finalOutputAdjusted = []
        for i in range(len(self.LAYERS)):
            activation = []
            derivative = []
            myInput = None
            
            #hidden and final layers
            if(i):
                myInput = np.array(activations[i-1])
            
            #input layer
            else:
                myInput = np.array(inputs)
                
            myInput = np.insert(myInput, 0, 1.0)
           
            
            myOutput = np.matmul(self.LAYER_MATRICES[i], myInput)
                
            for element in myOutput:
                if(self.useTanH):
                    activation.append(self.tanhActivation(element))
                    derivative.append(self.tanhDerivative(element))
                    
                else:
                    activationResult = self.sigmoidActivation(element) 
                    activation.append(activationResult)
                    derivative.append(self.sigmoidDerivative(activationResult))
                
            activations.append(activation)
            derivatives.append(derivative)
            
        
        finalOutput = activations[len(self.LAYERS)-1]
        finalOutputAdjusted = []
        
        for element in finalOutput:
            
            if(self.useTanH):
                if(element >= self.TANH_HIGH):
                    finalOutputAdjusted.append(1)
                elif(element <= self.TANH_LOW):
                    finalOutputAdjusted.append(-1)
                else:
                    finalOutputAdjusted.append(element)
            else:
                if(element >= self.SIGMOID_HIGH):
                    finalOutputAdjusted.append(1)
                elif(element <= self.SIGMOID_LOW):
                    finalOutputAdjusted.append(0)
                else:
                    finalOutputAdjusted.append(element)
          
        identifiedCategory = None                      
        if(self.useClassifyMode):
            identifiedCategory = np.argmax(finalOutput)
        
        return(derivatives, activations, finalOutput, finalOutputAdjusted, identifiedCategory)
        
        
    
        
    def backPropagate(self, derivatives, activations, errorVector, inputs):
        layersDeltas = []
        weightChangeMatrices = []
        for i in range(len(self.LAYERS)):
            preliminaryLayerDeltas = None
            #set index to the position of the last layer to work backwards
            index = len(self.LAYERS)-(i+1)
            
            #if this is not the final layer
            if (i):
                prevLayerDeltas = np.array(layersDeltas[i-1])
                
                #preliminaryLayerDeltas = np.matmul(prevLayerDeltas, np.delete(self.LAYER_MATRICES[index+1], 0, 1))

                preliminaryLayerDeltas = np.matmul((np.delete(self.LAYER_MATRICES[index+1], 0, 1)).T, prevLayerDeltas)
    
            #if dealing with the final layer    
            else:
                preliminaryLayerDeltas = np.array(errorVector)
    
            #create an array to hold the deltas for a layer
            layerDeltas = []
            
            #create an array to hold the weight changes for a single neuron
            #this array should be thought of as horizontal
            weightChanges = []
            
            #for each neuron in the layer
            for j in range(len(preliminaryLayerDeltas)):
                
                #the delta for this specific neuron is the sum of the weighted
                #deltas of all the neurons it feeds into times the derivative
                #of the neuron's activation function 
                individualDelta = preliminaryLayerDeltas[j]*derivatives[index][j]
                
                #append the value to the list of deltas for this layer
                layerDeltas.append(individualDelta)
                
                #if this is not the first layer of neurons
                if(index):
                    #the inputs to this neuron were the outputs of the layer before it
                    inputActivations = np.array(activations[index-1])
                    
                #if this is the fist layer of neurons
                else:
                    #the inputs to this neuron were the inputs to the network
                    inputActivations = np.array(inputs)
                    
                #insert 1 to the activations because of the 0th input, the bias, always 1
                inputActivations = np.insert(inputActivations, 0, 1)
                
                #the weight change for the neuron should be its delta times
                #eta multiplied elementwise with all of the inputs to the neuron
                weightChange = individualDelta*self.ETA*inputActivations
                weightChanges.append(weightChange)
            
            #form a matrix by stacking all the weight change arrays together
            weightChangeMatrix = np.stack(weightChanges)
            layersDeltas.append(layerDeltas)
            
            weightChangeMatrices.append(weightChangeMatrix)
            
        weightChangeMatrices.reverse()
        
        return(weightChangeMatrices)
    
    
    def randomizeData(self):
        currentTrainingPoints = []
        currentTrainingAnswers = []
        trainingCopy = [list(self.trainingSet[0]).copy(), list(self.trainingSet[1]).copy()]
        for i in range(len(self.trainingSet[1])):
            index = random.randint(0, len(trainingCopy[1])-1)
            currentTrainingPoints.append(trainingCopy[0].pop(index))
            currentTrainingAnswers.append(trainingCopy[1].pop(index))
        
     
        return(currentTrainingPoints, currentTrainingAnswers)
        
    def getTestError(self):
        errorAccumulator = 0    
        
        for j in range(len(self.testSet[1])):
            
            imageToRun = self.testSet[0][j]
            
            if(self.addNoise):
               imageToRun = self.generateNoise(imageToRun)
        
            
            
            result = self.forwardPropagate(imageToRun)
    
            correctAnswer = self.testSet[1][j]
            identifiedCategory = result[4]
            finalOutput = result[2]
 
            
                                      
            if(self.useClassifyMode):

                #increment the error accumulator with 1 if the point isn't correctly identified
                if(identifiedCategory != correctAnswer):
                    errorAccumulator += 1
            else:
                #calculate the error for each poin in the output using approximation
                errorVector, J2Loss = self.calculateLastLayerDeltasJ2Approximator(finalOutput, correctAnswer)
                errorAccumulator += J2Loss
        
        print(errorAccumulator)
        error = (errorAccumulator/len(self.testSet[1]))
        
        return(error)
    
    def getConfusionMatrixNumberClassification(self, training):
        confusionMatrix = np.zeros((10,10), dtype=int)
        dataset = None
        if(training):
            dataset = self.trainingSet
        else:
            dataset = self.testSet
        
        for j in range(len(dataset[1])):
            
            imageToRun = dataset[0][j]
            if(self.addNoise):
               imageToRun = self.generateNoise(imageToRun)
    
            result = self.forwardPropagate(imageToRun)
            
            correctAnswer = dataset[1][j]
            identifiedCategory = result[4]
            confusionMatrix[correctAnswer][identifiedCategory] += 1
            
        return(confusionMatrix)
            
            
    def generateInputOutput(self):
        selectedIndex = random.randint(0, len(self.testSet[0])-1)
        imageToRun = self.testSet[0][selectedIndex]
        imageToRunNoNoise = imageToRun.copy()
        if(self.addNoise):
           imageToRun = self.generateNoise(imageToRun)

        result = self.forwardPropagate(imageToRun)
        output = result[2]
        
        return(imageToRun, output, imageToRunNoNoise)
        
    def generateNeuronFeatureSet(self, neuronList, layerSelection):
        desiredWeights = []
        layerMatrix = self.LAYER_MATRICES[layerSelection].copy()

        for i in neuronList:
            desiredWeights.append(np.delete(layerMatrix[i], 0))
        
        return(desiredWeights)
    
    def getNumberErrorTrainingTest(self, trainingLabels, testLabels):
        
        errorAccumulatorTraining = np.empty(10)    
        
        for j in range(len(self.trainingSet[1])):
            imageToRun = self.trainingSet[0][j]
            if(self.addNoise):
               imageToRun = self.generateNoise(imageToRun)

            result = self.forwardPropagate(imageToRun)
                
            correctAnswer = self.trainingSet[1][j]
            identifiedCategory = result[4]
            finalOutput = result[2]

    
                                      
            if(self.useClassifyMode):
                    #increment the error accumulator with 1 if the point isn't correctly ident'ified
                    if(identifiedCategory != correctAnswer):
                        errorAccumulatorTraining[trainingLablels[j]] += 1
            else:
                #calculate the error for each poin in the output using approximation
                errorVector, J2Loss = self.calculateLastLayerDeltasJ2Approximator(finalOutput, correctAnswer)
                errorAccumulatorTraining[trainingLabels[j]] += J2Loss
        
        errorAccumulatorTraining /= 400
        
        
        errorAccumulatorTest = np.empty(10)  
      
        for j in range(len(self.testSet[1])):
            
            imageToRun = self.testSet[0][j]
            if(self.addNoise):
               imageToRun = self.generateNoise(imageToRun)

            result = self.forwardPropagate(imageToRun)
            
            correctAnswer = self.testSet[1][j]
            identifiedCategory = result[4]
            finalOutput = result[2]

    
                                      
            if(self.useClassifyMode):
                    #increment the error accumulator with 1 if the point isn't correctly identified
                    if(identifiedCategory != correctAnswer):
                        errorAccumulatorTest[testLabels[j]] += 1
            else:
                #calculate the error for each poin in the output using approximation
                errorVector, J2Loss = self.calculateLastLayerDeltasJ2Approximator(finalOutput, correctAnswer)
                errorAccumulatorTest[testLabels[j]] += J2Loss
        
        errorAccumulatorTest /= 100
        print(errorAccumulatorTest)
        #print(testLabels)
        
        return(errorAccumulatorTraining, errorAccumulatorTest)

    
    
    def train(self, numEpochs):
        trainingErrors = []
        testErrors = []
        offset = 0
        currentTestError = None
        currentTrainingPoints, currentTrainingAnswers = self.randomizeData()

        for k in range(numEpochs):
            
            if(not(self.useClassifyMode)):
                if(not(self.epochNumber%75) or self.epochNumber == 1):
                    inputImg, output, noNoise = self.generateInputOutput()
                    displayNetOutput1(inputImg, output, self.epochNumber, self.addNoise, noNoise)
           
            
            #check if the test error has started to increase while the training error has not, indicating overfitting
            if(not(self.epochNumber % self.testInterval)):
                currentTestError = self.getTestError()
                testErrors.append(currentTestError)
                if(currentTestError <=  min(testErrors)):
                    self.CURRENT_BEST = self.LAYER_MATRICES
                    
                if(self.epochNumber > self.testInterval*2):
                    testErrorChange =  np.mean(testErrors[-2:]) - np.mean(testErrors[-4:-2])
                    firstMin = int(math.floor(self.testInterval/2))
                    secondMin = -3*self.testInterval
                    secondMax = -3*self.testInterval + int(math.floor(self.testInterval/2))
                    
                    trainingErrorChange = np.mean(trainingErrors[firstMin:-1]) - np.mean(trainingErrors[secondMin:secondMax])
                    
                    print(trainingErrorChange, testErrorChange)
                                                                                                                                        
                    if(testErrorChange > 0 and trainingErrorChange < 0 and trainingError < self.learningGoal*3):
                        self.LAYER_MATRICES = self.CURRENT_BEST
                        print("All done, stopped overfit. Test error:", currentTestError, "Goal:", self.learningGoal, "Training:", trainingError)
                        return(trainingErrors, testErrors)

                
            #check if the test error is below the learning goal
            if(currentTestError <= self.learningGoal):
                print("All done. Test error:", currentTestError, "Goal:", self.learningGoal, "Training:", trainingError)
                self.LAYER_MATRICES = self.CURRENT_BEST
                return(trainingErrors, testErrors)
            
            
            
            weightChangesMomentum = None
            weightChangesAccumulator = None
            if(self.useMomentum):
                weightChangesMomentum = Layer.getWeightChangeBlank(self.LAYERS, self.INPUTS).copy()
            
                
            if(not(self.useOnline)):
                weightChangeAccumulator = Layer.getWeightChangeBlank(self.LAYERS, self.INPUTS).copy()
                    
            #the error accumulator variable is used to accumulate error for classification
            #as well as for approximation; error for each point is kept track of and
            #divided by the total number of points to get the value for an epoch
            errorAccumulator = 0    
        
        
            #if the offset for the batch range has reached the end of the training set,
            #randomize the order of the training set and set the offset back to the 
            #beginning, see not below for more context
            if(offset == len(self.trainingSet[1])):
                currentTrainingPoints, currentTrainingAnswers = self.randomizeData()
                offset = 0
                
            
            
            
            #train on a certain number of points, in batch mode, this represents a certain
            #batch of points in the training set, which batch range moves after each
            #batch until the end of the training set is reached and the points need
            #to be randomized again
            for j in range(self.BATCH_POINTS):
                selectedIndex = j+offset
                
                #result consists of (derivatives, activations, finalOutput, finalOutputAdjusted, identifiedCategory)
                
                imageToRun = currentTrainingPoints[selectedIndex]
                if(self.addNoise):
                   imageToRun = self.generateNoise(imageToRun)

                result = self.forwardPropagate(imageToRun)
               
                    
                correctAnswer = currentTrainingAnswers[selectedIndex]
                
                #set local variables with return values
                derivatives = result[0]
                activations = result[1]
                finalOutput = result[2]
                finalOutputAdjusted = result[3]
                identifiedCategory = result[4]
                
                
                errorVector = None
                if(self.useClassifyMode):
                    #calculate the error vector for each point in the output using classify mode       
                    errorVector = self.calculateLastLayerDeltasClassifier(finalOutputAdjusted, finalOutput, correctAnswer) 
                    
                    #increment the error accumulator with 1 if the point isn't correctly identified
                    #this reduces the total number of variables this complicated algorithm needs to use
                    if(identifiedCategory != correctAnswer):
                        errorAccumulator += 1
                else:
                    #calculate the error for each poin in the output using approximation
                    errorVector, J2Loss = self.calculateLastLayerDeltasJ2Approximator(finalOutput, correctAnswer)
                    errorAccumulator += J2Loss
                    
                #back propagate the error
                #this is where the index bug was, which was causing it to actually use currentTrainingPoints[j]
                #instead of currentTrainingWeights[selectedIndex], now fixed, but was likely behind the poor convergence
                weightChangeMatrices = self.backPropagate(derivatives, activations, errorVector, imageToRun)
                
                
                
                #if using online mode, immediately update the weights
                if(self.useOnline):
                    for m in range(len(self.LAYER_MATRICES)):
                        if m in self.trainLayers:
                            self.LAYER_MATRICES[m] = self.LAYER_MATRICES[m]+weightChangeMatrices[m] 
                 
                #else weights will be update after the batch, which can be the entire training set if the 
                else:
                    if(self.useMomentum):
                        for i in range(len(weightChangesMomentum)):
                            weightChangesMomentum[i] = weightChangeMatrices[i] + self.ALPHA*weightChangesMomentum[i]
                        for l in range(len(self.LAYER_MATRICES)):
                            weightChangeAccumulator[l] = weightChangeAccumulator[l]+weightChangesMomentum[l]
                    else:
                        for l in range(len(self.LAYER_MATRICES)):
                            weightChangeAccumulator[l] = weightChangeAccumulator[l]+weightChangeMatrices[l]
            
            if(not(self.useOnline)):
                for j in range(len(self.LAYER_MATRICES)):
                    if j in self.trainLayers:
                        self.LAYER_MATRICES[j] = self.LAYER_MATRICES[j]+weightChangeAccumulator[j]
                
              
            offset += self.BATCH_POINTS
        
            
            trainingError = (errorAccumulator/self.BATCH_POINTS)
            trainingErrors.append(trainingError)
            
            
            
            
            
                
            
            
            print(self.epochNumber, trainingError, currentTestError)
            self.epochNumber +=1
        
        self.LAYER_MATRICES = self.CURRENT_BEST
        return(trainingErrors, testErrors)
            
        


    
    

